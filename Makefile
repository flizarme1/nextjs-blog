create-network:
	docker network create db-connection || true

build-api:
	docker stop nextjs || true
	docker rm nextjs || true
	docker build -t nextjs .
	docker run -d --name nextjs -v `pwd`:/usr/src/app -v /usr/src/app/node_modules -p 3000:3000 --network=db-connection nextjs

create-stack:
	make create-network
	make build-api
