FROM node:alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

ADD package.json /usr/src/app
ADD yarn.lock /usr/src/app

# Production use node instead of root
# USER node

RUN yarn install

ADD . /usr/src/app

CMD [ "yarn", "dev" ]